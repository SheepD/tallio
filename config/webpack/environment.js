const { environment } = require('@rails/webpacker')

environment.loaders.set('styl', {
  test: /\.styl$/,
  loader: ['style-loader', 'css-loader', 'stylus-loader']
})

module.exports = environment
