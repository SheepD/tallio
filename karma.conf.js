const webpack = require('./config/webpack/test.js')

const tests = [
  'spec/javascripts/**/*_spec.js',
  'spec/javascripts/**/*_spec.jsx'
]

const files = tests.map(glob => ({ pattern: glob, watched: false }))
const preprocessors = {}
tests.forEach(glob => (preprocessors[glob] = ['webpack', 'sourcemap']))

module.exports = function (config) {
  config.set({
    reporters: ['progress'],
    frameworks: ['jasmine'],
    browsers: ['jsdom'],
    files,
    preprocessors,
    webpack
  })
}
