class TodoList < ApplicationRecord
  has_many :todo_list_items
  alias items todo_list_items
end
