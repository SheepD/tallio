class TodoListItem < ApplicationRecord
  belongs_to :todo_list
  alias list todo_list
end
