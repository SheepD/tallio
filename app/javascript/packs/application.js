/* eslint no-console:0 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import App from '../components/App'

Vue.use(VueRouter)
Vue.use(Vuetify)

document.addEventListener('DOMContentLoaded', () => {

  document.body.appendChild(document.createElement('app'))
  new Vue(App).$mount('app')
})
