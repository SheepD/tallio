class CreateTodo < ActiveRecord::Migration[5.1]
  def change
    create_table :todo_lists do |t|
      t.string :name
    end

    create_table :todo_list_items do |t|
      t.references :todo_list
      t.string :name
    end
  end
end
